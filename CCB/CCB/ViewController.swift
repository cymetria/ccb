//
//  ViewController.swift
//  Plaza Mayor Beacon
//
//  Created by Giovanny Alberto Suarez Portilla on 26/05/15.
//  Copyright (c) 2015 Multimedia Lab SAS. All rights reserved.
//

import UIKit
import CoreLocation


class ViewController: UIViewController, CLLocationManagerDelegate, ConnectionViewControllerDelegate, Connection2ViewControllerDelegate, Connection3ViewControllerDelegate {
    
    
    let locationManager = CLLocationManager()
    let region = CLBeaconRegion(proximityUUID: UUID(uuidString: "f7826da6-4fa2-4e98-8024-bc5b71e0893e")!, identifier: "Kontakt")
    //let region = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "b9407f30-f5f8-466e-aff9-25556b57fe6d")!, identifier: "Estimotes")
    var vc:AdviseViewController!
    var vc2:Advise2ViewController!
    var vc3:Advise3ViewController!
    var flag = false
    
    func activateBeacon() {
        locationManager.startRangingBeacons(in: region)
    }
    func activate2Beacon() {
        locationManager.startRangingBeacons(in: region)
    }
    func activate3Beacon() {
        locationManager.startRangingBeacons(in: region)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        vc = self.storyboard?.instantiateViewController(withIdentifier: "advise") as! AdviseViewController
        vc.delegate = self
        
        vc2 = self.storyboard?.instantiateViewController(withIdentifier: "advise2") as! Advise2ViewController
        vc2.delegate = self
        
        vc3 = self.storyboard?.instantiateViewController(withIdentifier: "advise3") as! Advise3ViewController
        vc3.delegate = self
        
        locationManager.delegate = self
        
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse) {
            locationManager.requestWhenInUseAuthorization()
        }
        locationManager.startRangingBeacons(in: region)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {

        let knownBeacons = beacons.filter{ $0.proximity == CLProximity.immediate}
        

        
        if (knownBeacons.count > 0) {
            flag = true
            locationManager.stopRangingBeacons(in: region)
        }
        
        if (flag == true){
            let beaconProx:CLBeacon = knownBeacons[0]
            print(beaconProx.major)
            if (beaconProx.major == 19577){
                self.present(vc as AdviseViewController, animated: true, completion: nil)
                vc.runBeacon()
            } else if (beaconProx.major == 30101){
                self.present(vc2 as Advise2ViewController, animated: true, completion: nil)
                vc2.runBeacon()
            } else if (beaconProx.major == 49690){
                self.present(vc3 as Advise3ViewController, animated: true, completion: nil)
                vc3.runBeacon()
            }


            flag = false
            
        }
    }
    
}


//
//  AdviseViewController.swift
//  Plaza Mayor Beacon
//
//  Created by Giovanny Alberto Suarez Portilla on 26/05/15.
//  Copyright (c) 2015 Multimedia Lab SAS. All rights reserved.
//

import UIKit
import CoreLocation

protocol ConnectionViewControllerDelegate {
    
    func activateBeacon()
    
}


class AdviseViewController: UIViewController, CLLocationManagerDelegate {
    
    var delegate:ConnectionViewControllerDelegate?
    
    let locationManager = CLLocationManager()
     let region = CLBeaconRegion(proximityUUID: UUID(uuidString: "f7826da6-4fa2-4e98-8024-bc5b71e0893e")!, identifier: "Kontakt")
    var vc:AnyObject!
    var flag = false
    
    func runBeacon() {
        locationManager.startRangingBeacons(in: region)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        vc = self.storyboard?.instantiateViewController(withIdentifier: "advise") as! AdviseViewController
        
        
        // Do any additional setup after loading the view, typically from a nib.
        locationManager.delegate = self
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse) {
            locationManager.requestWhenInUseAuthorization()
        }
        locationManager.startRangingBeacons(in: region)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        let knownBeacons = beacons.filter{ $0.proximity == CLProximity.immediate }
        
        print(beacons)
        if (knownBeacons.count == 0) {
            flag = true
            locationManager.stopRangingBeacons(in: region)
        }
        
        if (flag == true){
            delegate?.activateBeacon()
            self.dismiss(animated: true, completion: nil)
            flag = false
            
        }

    }
}
